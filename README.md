# Kafka install role
## Install
### Install python (ubuntu)
For use ansible, you need install python2.7 on remote server.
```bash
ansible kafka -m raw -a "apt-get -qq update;\
apt-get -y --no-install-recommends install python2.7;"
```

### ansible.cfg example
```bash
cat <<'EOF' > ansible.cfg
[defaults]
hostfile = hosts
remote_user = root
deprecation_warnings = False
roles_path = roles
force_color = 1
retry_files_enabled = False
executable = /bin/bash

[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no
pipelining = True
EOF
```

### Download this roles
```bash
cat <<'EOF' >> req.yml

- src: git+https://gitlab.com/kapuza-ansible/kafka.git
  name: kafka
EOF
echo "roles/kafka" >> .gitignore
ansible-galaxy install --force -r ./req.yml
```

## Kafka (ubuntu)
```bash
cat << 'EOF' > kafka.yml
---
- hosts:
    kafka
  roles:
    - role: kafka
      service_start: true
      kafka_ver: '2.12-1.0.1'
EOF

ansible-playbook kafka.yml
```
